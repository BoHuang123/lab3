.data 0x10010000
# reserve space in memory for varibles and asign values 
my_array: .space 40
initial_value: .word 1

.text
.globl main
main: addu $s0,$ra, $0

lw $t0, initial_value   #put the initial_value into $t0
li $t1,0
la $t9, my_array  #put the address of my_arry into $t9

loop:
beq $t1,10, exit   #if $t1 hit 10, then exit
sw $t0,0($t9)   #put $t0 to my_array
addi $t1,$t1,1  #i++
addi $t0, $t0,1 #initial_value++
addi $t9, $t9,4 #goto the next element in my_array
j loop

exit:


addu $ra,$0, $s0
jr $ra


