.data 0x10010000
# reserve space in memory for varibles and asign values 
var1: .word 1   



.text
.globl main
main: addu $s0,$ra, $0

lw $t1, var1
addu $t2, $0, $t1
li $t5, 100

loop:
#beq $t2,$t5,exit
ble $t5,$t2,exit
addi $t1,$t1,1
addi $t2, $t2,1
j loop
 
exit:
sw $t1, var1

addu $ra,$0, $s0
jr $ra


