.data 0x10010000
# reserve space in memory for varibles and asign values 
var1: .word 1   
var2: .word 5
var3: .word -2020


.text
.globl main
main: addu $s0,$ra, $0

lw $t1,var1
lw $t2,var2
lw $t3, var3

bne $t1,$t2, Else
sw $t3,var1
sw $t3,var2
beq $0, $0, Exit

Else:
move $t0, $t1
move $t1, $t2
move $t2, $t0
sw $t1,var1
sw $t2,var2

Exit:

li $v0,1
lw $a0, var1
syscall
lw $a0, var2
syscall

addu $ra,$0, $s0
jr $ra


