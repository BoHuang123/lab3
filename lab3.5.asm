.data 0x10010000
# reserve space in memory for varibles and asign values 
msg1:  .asciiz "Please enter an integer number: "
msg2:  .asciiz "Please enter another integer number: "
msg3:  .asciiz "I'm far away"
msg4:  .asciiz "I'm nearby"


.text
.globl main
main: addu $s0,$ra, $0

#read the 1st integer into $t0
li $v0, 4   #print the prompts
la $a0, msg1
syscall

li $v0, 5 #get the input from user
syscall


move $t0,$v0  #put user input in $t0

#read the 2nd integer into $t1
li $v0, 4
la $a0, msg2
syscall

li $v0, 5
syscall


move $t1,$v0


beq $t1, $t0, Far  #check if the two integers are different, if they are same, goto Far
li $v0, 4  #if they are different
la $a0, msg4  #print "i am nearby"
syscall
beq $0,$0,end

Far:
li $t8,0x10010000 #put an address which is far away in an register
li $v0, 4
la $a0, msg3
syscall
jr $t8  #jump to that far away address
beq $0,$0,end


end:
li $v0, 10
syscall

addu $ra,$0, $s0
jr $ra


